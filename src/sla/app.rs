use crate::database::*;
use crate::user::*;

#[tokio::main]
pub async fn run() -> mongodb::error::Result<()> {
    let client = connect();
    let mut new_user = User::new();
    new_user.name_input();
    new_user.email_input();
    new_user.password_input();
    insert_user(&client.unwrap(), &new_user) 
        .await?;
    Ok(())
}
