use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct User {
    pub name: String,
    pub email: String,
    pub password: String,
}

impl User {
    pub fn new() -> Self {
        User {
            name: String::new(),
            email: String::new(),
            password: String::new(),
        }
    }
}
