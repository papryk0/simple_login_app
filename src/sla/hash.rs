use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub fn hash(input: &str) -> String {
    let mut hasher = DefaultHasher::new();
    input.hash(&mut hasher);
    let hash_value = hasher.finish();
    format!("{:x}", hash_value)
}