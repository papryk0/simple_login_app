use sla_lib::app;

#[tokio::main]
async fn main() -> mongodb::error::Result<()> {
    app::run()?;
    Ok(())
}